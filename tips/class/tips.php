<?php

class Tips{

	public function clean($str){

		$string = str_replace(' ', '-', $str);

		return strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $str)); 

	}

	public function synchronize(){

		$id = json_decode(file_get_contents('php://input'))->project_id;

		if($id == 12425974){

			shell_exec("git pull");
		}
	}


	public function getTitles($html){

		$doc = new DOMDocument();
		$doc->loadHTML($html);
		$doc->saveHTML();
		$titles = $doc->getElementsByTagName("h3");
		$back = [];

		foreach($titles as $title){

			$back [] = $title->nodeValue;

		}

		return $back;
	}

	public function all(){

		$parse = new Parsedown();
		$files = glob("content/*.md");
		$tips = [];

		foreach($files as $file){

			$tips [pathinfo($file)['filename']] = $parse->text(file_get_contents($file));
		}

		return $tips;
	}
}

