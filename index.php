<?php require("tips/launch.php") ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/styles/vs.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/main.css">
	<title>Tips from Luuse</title>
</head>
<body>
	<header>
		<div class="title rainbow">
			<h1>⚗️ TIPS.LUUSE.IO 🖥️</h1>
			<h2>Code that might work</h2>
		</div>
		<nav>
			<?php foreach($tips->all() as $id=>$tipList): ?>
				<div class="lang" data-id="<?= $id ?>">
					<a href="#<?= $id ?>"><?= ucfirst($id) ?></a>
					<div class="tips">
						<?php foreach($tips->getTitles($tipList) as $subnum=>$title): ?>
							<a href="#<?= $tips->clean($title) ?>" data-num="<?= $subnum ?>" data-link="<?= $tips->clean($title) ?>">
								<?= $title ?>
							</a>
						<?php endforeach ?>	
					</div>
				</div>
			<?php endforeach ?>	
		</nav>
	</header>
	<main>
		<section class="all">
			<?php foreach($tips->all() as $id=>$tipList): ?>
				<div class="lang" id="<?= $id ?>">
					<?= $tipList ?>
				</div>
			<?php endforeach ?>
		</section>
		<section class="logitheque">
		 <details>
		 <summary>Our favorite softwares</summary>
			 <fieldset>
				<?= $logitheque ?>
			 </fieldset>
		</details>
		</section>
	</main>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>
<script type="text/javascript" src="assets/js/main.js"></script>
</html>
