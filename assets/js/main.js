function setSubAnchors(){

	var languages = document.querySelectorAll(".all .lang");

	languages.forEach((language) => {

		var lis = language.querySelectorAll("li");
		var id = language.id;

		lis.forEach((li, index) => {

			li.id=document.querySelector("nav [data-id='"+id+"'] a[data-num='"+index+"']").dataset.link;

		});

	});
}


function openCloseMenu(){

	var links = document.querySelectorAll("nav .lang>a");

	links.forEach((link) => {


		link.addEventListener("click", () => {

			var lang = link.parentElement;

			if(lang.classList.contains("open")){

				lang.classList.remove("open");

			}else{

				lang.classList.add("open");
			}

		});

	});
}

function logitheque(){
	let btnLogi = document.getElementById('btn-logi')

	btnLogi.addEventListener('click', () => {
		alert('yess')

	})
}

document.addEventListener("DOMContentLoaded", () => {

	openCloseMenu();
	setSubAnchors();
	logitheque();

});
