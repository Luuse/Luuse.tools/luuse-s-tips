## Rsync 
-   ### Envoyer un fichier sur un server 
    ``` bash
    $ rsync myfile.html user@exemple.com:/server/path/ -e 'ssh -p 444'
    ```
-   ### Envoyer un dossier sur un server 
    ``` bash
    $ rsync -r myfolder/ user@exemple.com:/server/path/ -e 'ssh -p 444' 
    ```
-   ### Récupérer un fchier sur un server
    ``` bash
    $ rsync user@exemple.com:/server/path/myfile.html /local/path/ -e 'ssh -p 444'
    ```
-   ### Récupérer un dossier sur un server 
    ``` bash
    $ rsync -r user@exemple.com:/server/path/myfolder/ /local/path/ -e 'ssh -p 444' 
