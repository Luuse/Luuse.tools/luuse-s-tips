## encfs & fusermount

	* Intall -> fuse-utils encfs   
	* encfs - mounts or creates an encrypted virtual filesystem 
	* fusermount - mount and unmount FUSE filesystems  

- ### Create new folders.
		´encfs /home/$USER/coffre/ /home/$USER/coffre_open/´

- ### Unmount and encrypt folder.
		´fusermount -u /home/$USER/coffre_open´

- ### Mount and decrypt folder.
		'encfs /home/$USER/coffre/ /home/$USER/coffre_open/'
