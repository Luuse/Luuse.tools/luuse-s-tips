## ssh
- ### SCP : Transfert de fichiers entre machines
Du PC local vers un PC distant
``` bash
$ scp chemin/local/fichier-envoi user@192.168.1.x:chemin/de/réception/ 
```
Du PC Distant vers le PC local 
``` bash
$ scp user@192.168.1.x:chemin/distant1/fichier-envoi chemin/local/de/reception 
```
Du PC distant vers un autre PC distant 
``` bash
$ scp user@192.168.1.x:chemin/distant1/fichier-envoi 192.168.1.x:chemin/distant2/de/reception 
```
En cas de port personnalisé... 
``` bash
$ scp -P 12345 chemin/origine/fichier user@192.168.1.x:chemin/de/réception 
```
