## Php
- ### Run php without apache
	In your shell: 
	``` bash
	php -S localhost:8000
	```
	if you want start at a different root:
	``` bash
	php -S localhost:8000 -t </path/to/root>
	```
- ### Slugify
  ``` php
  
  public function slugify($string){

    $string = str_replace(array('[\', \']'), '', $string);
    $string = preg_replace('/\[.*\]/U', '', $string);
    $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
    $string = htmlentities($string, ENT_COMPAT, 'utf-8');
    $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
    $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);

    return strtolower(trim($string, '-'));
  }
  ```
- ### Regex for type (french)
  ``` php
  function rft($content) {
  	$thinsp = '<span class="thinsp">&nbsp;</span>';
  	$regex = array(
    	"/ ;/"	  => ';',
      "/« /"	  => '«',
    	"/“ /"	  => '«',
    	"/ »/"	  => '»',
    	"/ ”/"	  => '»',
    	"/ :/"	  => ':',
    	"/ !/"	  => '!',
      "/ €/"    => '€',
      "/ $/"    => '$',
      "/ £/"    => '£',
    	"/ \?/"	  => '?',
    	"/:/" 	  => $thinsp . ':',
      "/«/"     => '«' . $thinsp,
    	"/“/" 	  => '«' . $thinsp,
      "/»/" 	  => $thinsp . '»',
      "/€/"     => $thinsp . '€',
      "/$/"     => $thinsp . '$',
      "/£/"     => $thinsp . '£',
    	"/”/" 	  => $thinsp . '»',
    	"/!/" 	  => $thinsp . '!',
    	"/\?/"	  => $thinsp . '?',
    	"/& /"	  => '&amp; ',
    	"/oe/"	  => '&#339;',
    	"/ae/"	  => '&#230;',
    	"/OE/"	  => '&#338;',
    	"/AE/"	  => '&#198;',
    	"/\.\.\./"=> '&#8230;',
        # url bug correction
    	'/http<span class="thinsp">&nbsp;<\/span>:/' => 'http:',
    	'/https<span class="thinsp">&nbsp;<\/span>:/' => 'https:',
    	'/href="mailto<span class="thinsp">&nbsp;<\/span>:/' => 'href="mailto:',
    	'/watch<span class="thinsp">&nbsp;<\/span>?/' => 'watch',
   });
      
    foreach ($regex as $input => $output) {
  		$content = preg_replace($input, $output, $content);
      }
      return $content;
    }
  ```

  Then use the css property `letter-spacing` on `.thinsp` to set the width value of your thinspace. 

- ### Regex for type (english)
  ``` php
  function rft($content) {
    $thinsp = '<span class="thinsp">&nbsp;</span>';
    $regex = array(
        "/ ;/"    => ';',
        "/« /"    => '«',
        "/“ /"    => '“',
        "/ »/"    => '»',
        "/ ”/"    => '”',
        "/ :/"    => ':',
        "/ !/"    => '!',
        "/ €/"    => '€',
        "/ $/"    => '$',
        "/ £/"    => '£',
        "/ \?/"   => '?',
        "/«/"     => '«' . $thinsp,
        "/»/"     => $thinsp . '»',
        "/€/"     => $thinsp . '€',
        "/$/"     => $thinsp . '$',
        "/£/"     => $thinsp . '£',
        "/& /"    => '&amp; ',
        "/oe/"    => '&#339;',
        "/ae/"    => '&#230;',
        "/OE/"    => '&#338;',
        "/AE/"    => '&#198;',
        "/\.\.\./"=> '&#8230;',
          # url bug correction
        '/http<span class="thinsp">&nbsp;<\/span>:/' => 'http:',
        '/https<span class="thinsp">&nbsp;<\/span>:/' => 'https:',
        '/href="<main></main>ilto<span class="thinsp">&nbsp;<\/span>:/' => 'href="mailto:',
        '/watch<span class="thinsp">&nbsp;<\/span>?/' => 'watch',
       });

      foreach ($regex as $input => $output) {
            $content = preg_replace($input, $output, $content);
      }
      return $content;
  }
  ```
Then use the css property `letter-spacing` on `.thinsp` to set the width value of your thinspace. 

