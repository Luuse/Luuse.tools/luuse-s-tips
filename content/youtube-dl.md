## youtube-dl

Download videos from youtube.com or other video platforms - [github](https://github.com/ytdl-org/youtube-dl)

- ### Download playlist from youtube
    ``` bash
    youtube-dl --extract-audio --audio-format mp3 -o "%(title)s.%(ext)s" <url to playlist>
    ```
