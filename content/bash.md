## Bash
-   ### Rename all files by number in a directory
    ``` bash
    $ ls | cat -n | while read n f; do mv "$f" `printf "%02d.png" $n`; done
    ```

-   ### Show Key Name 
	showkey - examine the codes sent by the keyboard 
	``` bash
		$ showkey -a 
	```
	``` bash output
		Press any keys - Ctrl-D will terminate this program
		a 	 97 0141 0x61
	```
-		### Grep  
Find content into directory files.
``` bash
	grep "some string" . -R
```
		
Another method for the same result.
``` bash
grep -rnw . -e  'some string'
```
-  ### Make File Executable
	``` bash 
	chmod u+x script.sh
	```
