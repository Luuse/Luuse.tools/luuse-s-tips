## Bootable usb (linux)
Voici le processus pour générer une clé usb d'installation d'un système Gnu/Linux.
Avant tout, il faut se procurer un `.iso` du système à installer et une clé USB avec un espace de stokage supérieur à cet `.iso`.

- ### Trouver le périphérique de la clé USB
	``` bash
 	$ sudo fdisk -l
	```
	``` bash output
	...
	Disque /dev/sdb : 7,2 GiB, 7763656704 octets, 15163392 secteurs
	Modèle de disque : USB DISK 2.0    
	Unités : secteur de 1 × 512 = 512 octets
	Taille de secteur (logique / physique) : 512 octets / 512 octets
	taille d'E/S (minimale / optimale) : 512 octets / 512 octets
	Type d'étiquette de disque : dos
	Identifiant de disque : 0x39d2fa1e
	...
	```
	Dans la réponse vous devez récupérer un chemin qui doit ressembler à quelque chose comme ça `/dev/sdx` dans cet exemple nous avons `/dev/sdb`.

- ### Générer la clé USB à partir de l'Iso
	``` bash
	$ dd if=image.iso of=/dev/sdb bs=4M && sync
	```
	Si cette opération ne fonctionne pas essayez de reformater la clé.
