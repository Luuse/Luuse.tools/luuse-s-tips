## ffmpeg
- ### Concatenate
    ``` 
    ffmpeg -i input1.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate1.ts
    ffmpeg -i input2.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate2.ts
    ffmpeg -i "concat:intermediate1.ts|intermediate2.ts" -c copy -bsf:a aac_adtstoasc output.mp4
    ```
- ### Extract Images at Framerate
    ``` 
    ffmpeg -i video.mp4 -vf fps=framerate output_images%02d.jpg
    ``` 
- ### Compresser les capures vidéos pour le sit de luuse
    ```  
    ffmpeg -i  mon-fichier-entrant.mp4 -b 1000000 mon-fichier-sortant.mp4
    ```  
- ### Mov to mp4
    ```  
    ffmpeg -i movie.mov -vcodec copy -acodec copy out.mp4
    ```
- ### 1.5 scaling
    ```
    ffmpeg -i output.mp4 -vf "scale=iw/1.5:ih/1.5" output3.mp4
    ```
- ### Cutting
    ```
    ffmpeg -ss 00:00:00.0 -i boctok.mp4 -c copy -t 00:01:18.0 cut2.mp4
    ```
- ### Get first frame -- same size
    ``` 
    ffmpeg -i bocto-site.mp4 -vf "select=eq(n\,0)" -q:v 3 bisous.jpg
    ```
- ### Get frame at time
    ``` 
    ffmpeg -ss 00:23:45 -i input -vframes 1 -q:v 2 output.jpg
    ``` 
    ```    
    ffmpeg -ss 0.5 -i inputfile.mp4 -t 1 -s 480x300 -f image2 imagefile.jpg
    
    ```
- ### Convert images to video
    ```
    ffmpeg -framerate 1 -pattern_type glob -i '*.jpg' -c:v libx264 -r 3 out.mp4
    ```

