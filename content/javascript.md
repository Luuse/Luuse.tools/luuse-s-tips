## Javascript
-  ### XMLHttpRequest	

``` js
var xhr = new XMLHttpRequest()
xhr.open('GET', 'https://your.site')
xhr.send(null)

xhr.onreadystatechange = function () {
	var DONE = 4
	var OK = 200
	if (xhr.readyState === DONE) {
		if (xhr.status === OK) {
			console.log('yes')
		} else {
			console.log('Error: ' + xhr.status)
		}
	}
}
```

