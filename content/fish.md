## fish
The friendly interactive shell. [website](https://fishshell.com/)
- ### Define Fish to default shell
	1. Shows the full path of `fish` with `which` or `whereis`:
	``` bash
	$ which fish
	> /usr/bin/fish
	```
	2. Now, change your login shell:
	``` bash
	chsh -s /usr/bin/fish
	```
- ### Alias 
	Add alias (exemple):
	``` bash
	$ alias x="exit"
	``` 
	Save alias:
	``` bash
	$ funcsave x
	``` 

