## Inotifywait 

**Inotifywait** efficiently waits for changes to files using Linux's inotify interface. It  is suitable for waiting for changes to files from shell scripts.  It can either exit once an event occurs, or continually execute and output events  as  they occur.

- ### Install

On Archlinux : `sudo pacman -Ss inotify-tools`.  
With npm : `npm install -g inotify`. 

- ### Exemples

If you want make an event with recursives files. `'.'` is the root path.

``` bash
#!/usr/bin/env bash

inotifywait -m -r '.' -e create -e moved_to |
while read path action file; do
  echo 'ACTION HERE!!';
done

```
