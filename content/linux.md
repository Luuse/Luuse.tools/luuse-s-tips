## Linux 
	* / => Racine, elle contient les répertoires principaux
	* /bin (binaries) => Exécutables essentiels au système, utilisables par tous les utilisateurs (ls pwd cp)
	* /boot => fichiers permettant à Linux de démarrer
	* /dev (device) => Fichiers spéciaux représentant les point d'entrées de tous les périphériques (fichiers spéciaux des disques dusr, écrans, partitions, consoles TTY, webcam)
	* /etc (editing text config) => Contient les fichiers texte nécessaires à la configuration du système et des setvices (XXX.conf, passwd, inittab, fstab)
	* /home => Répertoire personnel des utilisateurs
	* /lib (librairies) => contient les bibliothèques partagées essentielles au système lors du démarrage (et modules noyau)
	* /lib64 => idem /lib mais pour les 64bits (parfois, on trouvera lib et lib32. Dans ce cas, lib = 64bits et lib32 = 32bits)
	* /mnt (mount) /media => Là où les ressources peuvent être montées de manière permanente (/media) ou temporaire (/mnt)
	* /opt (optional) => Répertoire générique pour l'installation de programmes installés hors dépôts de la distribution.
	* /proc (process) => Répertoire virtuel ne prenant aucune place sur le disque. Contient des informations sur le système (noyau, processus).
	* /root => Répertoire personnel du super utilisateur (le répertoire de root n'est pas dans /home, car bien souvent le /home est sur une partition à part. En cas d'échec de montage de /home, root à quand même accès à son répertoire personnel).
	* /run (runtime system) => Contient des informations relatives au système concernant les utilisateurs et les services en cours d'exécution.
	* /sbin (super binaries) => Contient les programmes système essentiels utilisables par l'admin uniquement.
	* /srv (services) => N'est pas présent dans toutes les distributions. C'est un répertoire de données pour divers services (stockage des documents de comptes FTP, ou pages de sites web)
	* /sys => Répertoire virtuel ne prenant aucune place sur le disque. Contient des informations entre le système et ses composants matériels.
	* /tmp (temporary) => Répertoire fichier temporaires
	* /usr (Unix System Resources) => Contient des programmes installés (/usr/bin) avec leur librairies (/usr/lib ou /usr/lib64) tels que firefox, libreoffice, ... quelques programmes réservés à l'admin système (/usr/sbin) et les fichiers de code source (/usr/src). On y retrouve /usr/share avec les éléments partagés indépendants de l'architecture (documentation, icônes, ...). Dans /usr/local on pourra installer les programmes compilés manuellement sur le système.
	* /var (variable) => contient les données variables (fichiers de log dans /var/log) mais parfois les bases de données (/var/lib/mysql) et les pages de site web (/var/www/html)
