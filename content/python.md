## Bash
-   ### PDF imposition for printing booklet

    using pdfbook2

    > <https://github.com/jenom/pdfbook2>
    > requirements
    > Python 3.6, pdfjam <https://github.com/rrthomas/pdfjam>, pdfcrop and their dependencies.
    > --signature=8 (Define the signature for the booklet handed to pdfjam, needs to be multiple of 4 (default: 4))
    > --short-edge (opposite of long-edge, configuration dfor double-side printing)

    ``` bash
    pdfbook2 --paper=a4paper --no-crop --signature=8 --short-edge test.pdf
    ```

