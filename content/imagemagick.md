## Imagmagick  
-   ### Turn pngs into gifs
    ``` 
    convert -delay 60 *.png -loop 0 live3.gif convert rose.jpg -resize 80% rose.png 
    ```
- ### Pdf to png > all files in a directory
    ``` 
    mogrify -density 300 -format png *.pdf
    ```
- ### 72 dpi
    ``` 
    convert -units PixelsPerInch input.png -density 72 output.png
    ```
- ### resize %
    ``` 
    convert -resize 40% versions_44.png
    ```
- ### Blur img
    ``` 
    convert -resize 10% -resize 1000% output_image.jpg blured.jpg
    ```
- ### Monochrome Tinted Image > replace black with color
    ``` 
convert in.jpeg  -fuzz 30% -fill 'rgb(114, 98, 60)' -opaque black  out.jpeg
    ```