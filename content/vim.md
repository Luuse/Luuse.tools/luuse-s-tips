## Vim
- ### Replace.  
	
	#### Simple replace
	All in content
	``` vim
	:%s/paul/luc/g
	```  
	
	#### Remove all balise xml/html 
	``` vim
	:%s/<[^>]*>//g
	```  
	### Indent HTML in PHP file 
	To indent html in vim, select the lines then press `=`
	But in php file, change the filetype before with `:set filetype=html` 

