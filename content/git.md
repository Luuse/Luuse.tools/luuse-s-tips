## Git 
- ### Git Log
	#### Decoration
``` bash
git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all 
```
![git log color](content/images/git-log.png)
After you can build an [alias](http://www.luuse.io/tips/#fish).

- ### .gitignore Grav Project
    ``` git
.libsass.json
.DS_Store
*.min.css
*.css.map
accounts/*
plugins/*
*.png
*.PNG
*.jpg
*.JPG
*.jpeg
*.JPEG
*.GIF
*.gif
*.mp4
*.MP4
*.tiff
*.tif
*.TIFF


    ```